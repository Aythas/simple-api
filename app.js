var express= require('express')
var app=express()
var port=5000
app.get('/',function(req,res) {
    res.send('<h1>Hello World</h1>')
})
app.get('/weather',function(req,res)
{
    res.send({'forecast' : 'snowy',
              'location' : 'boston'})
})
app.listen(port, function(err){
    if(err) throw err
    console.log('server running on port'+ port)
})
